<?php namespace Medika\Joshua;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Medika\Joshua\Components\Cart' => 'Cart',
            'Medika\Joshua\Components\Catalog' => 'Catalog',
            'Medika\Joshua\Components\Invoices' => 'Invoices',
            'Medika\Joshua\Components\InvoiceFinal' => 'InvoiceFinal',
            'Medika\Joshua\Components\MySales' => 'MySales',
        ];
    }

    public function registerSettings()
    {
    }

    public function registerPermissions()
    {
        return [
            'medika.joshua.admin' => [
                'label' => 'Admin Permission',
                'tab' => 'Permission',
                'order' => 1,                    
            ],
            'medika.joshua.kasir' => [
                'label' => 'Kasir Permission',
                'tab' => 'Permission',
                'order' => 1,                    
            ]            
        ];
    }
}
