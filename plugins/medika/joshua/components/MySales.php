<?php namespace Medika\Joshua\Components;

use Cms\Classes\ComponentBase;
use Medika\Joshua\Models\Customer;
use Medika\Joshua\Models\Penjualan;
use Medika\Joshua\Models\LogAktifitas;
use Medika\Joshua\Models\PenjualanProduk;
use Auth;
use Redirect;

class MySales extends ComponentBase
{
    public $user;
    public $sales;
    public $sale;
    public $opsi;

    public function componentDetails()
    {
        return [
            'name'        => 'My Sales Component',
            'description' => 'List penjualan ku'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->user = Auth::getUser();

        $saleId = $this->param('tr_id');
        
        if (!$saleId){
            $this->sales = Penjualan::where('status', '=',0)
            ->where('user_id','=',$this->user->id)
            ->orderBy('updated_at','asc')->with('penjualanproduk','user','customer')
            ->get();
        }else{
            $this->sale = Penjualan::where('no_faktur', '=',$saleId)
            ->where('user_id','=',$this->user->id)
            ->orderBy('updated_at','asc')->with('penjualanproduk','user','customer')
            ->first();
            //dump($this->sale);
        }

        // if (count($this->sales) > 0 ){
            
        // }else{
        //     return Redirect::to('/penjualan');
        // }
        
        
            
    }
}
