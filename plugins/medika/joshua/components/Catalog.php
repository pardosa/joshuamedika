<?php namespace Medika\Joshua\Components;

use Cms\Classes\ComponentBase;
use Medika\Joshua\Models\Catalog as CatalogModel;
use Auth;
use Flash;

class Catalog extends ComponentBase
{
    public $products;
    private $user;
    public function componentDetails()
    {
        return [
            'name'        => 'Catalog Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->user = Auth::getUser();

        $query = post('query');
        $opsi = $this->param('opsi');

        if ($opsi == 'search'){
            $this->products = CatalogModel::where('nama','like','%'.$query.'%')->orderBy('kode','asc')->with('supplier','inventory')->paginate(12);
            $this->products->setPath('/penjualan/search/'.$query);
        }else{
            $this->products = CatalogModel::orderBy('kode','asc')->with('supplier','inventory')->paginate(12);
            $this->products->setPath('/penjualan');
        }
        
        
    }

    public function searchCatalog($search){
        //Flash::success('You did it!');
    }
}
