<?php namespace Medika\Joshua\Components;

use Cms\Classes\ComponentBase;
use Medika\Joshua\Models\Cart as CartModel;
use Medika\Joshua\Models\Customer;
use Medika\Joshua\Models\Penjualan;
use Medika\Joshua\Models\LogAktifitas;
use Medika\Joshua\Models\PenjualanProduk;
use Auth;
use Redirect;

class Invoices extends ComponentBase
{
    public $user;
    public $carts;
    public $customer;
    public $customers;
    public $noInvoice;
    public $tanggal;
    public $opsi;

    public function componentDetails()
    {
        return [
            'name'        => 'Invoices Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->user = Auth::getUser();

        $custId = $this->param('customer_id');
        $this->opsi = $this->param('opsi');

        $this->carts = CartModel::where('status', '=',0)
            ->where('user_id','=',$this->user->id)
            ->orderBy('updated_at','asc')->with('catalog','user')
            ->get();

        if (count($this->carts) > 0 ){
            $this->noInvoice = "JM-".$this->user->id . "-" .$this->carts[0]['created_at']->getTimestamp();
        }else{
            return Redirect::to('/penjualan');
        }
        
        $this->tanggal = now();

        if ($this->opsi == 'save'){
            $this->customer = Customer::find($custId);

            $penjualan = new Penjualan();
            $penjualan->no_faktur = $this->noInvoice;
            $penjualan->customer_id = $this->customer->id;
            $penjualan->user_id = $this->user->id;
            $penjualan->total = 0;
            $penjualan->status = 0;
            $penjualan->save();

            $total = 0;
            foreach ($this->carts as $key => $cart) {
                if ($cart->catalog->inventory->aktual >= $cart->jumlah){
                    $cart->catalog->inventory->aktual = $cart->catalog->inventory->aktual - $cart->jumlah;
                    $cart->catalog->inventory->save();

                    $prod = new PenjualanProduk();
                    $prod->penjualan_id = $penjualan->id;
                    $prod->catalog_id = $cart->catalog_id;
                    $prod->jumlah = $cart->jumlah;
                    $prod->harga_beli = $cart->catalog->modal;
                    $prod->harga_jual = $cart->catalog->harga;
                    $prod->total = $cart->catalog->harga * $cart->jumlah;
                    $prod->save();

                    $log = new LogAktifitas();
                    $log->kategori = "Penjualan Produk". $penjualan->no_faktur;
                    $log->user_id = $this->user->id;
                    $log->data = json_encode($prod);
                    $log->save();

                    $total = $total + $prod->total;
                }
            }
            $penjualan->total = $total;
            $penjualan->save();

            $log = new LogAktifitas();
            $log->kategori = "Penjualan ". $penjualan->no_faktur;
            $log->user_id = $this->user->id;
            $log->data = json_encode($penjualan);
            $log->save();

            $update = CartModel::where('status', '=',0)
                ->where('user_id','=',$this->user->id)
                ->orderBy('updated_at','asc')->with('catalog','user')
                ->update(['status' => 1]);

                return Redirect::to("/cetak-invoice/".$penjualan->id);
            

        }elseif($this->opsi == 'customer'){
            if ($custId != null){    
                $this->customer = Customer::find($custId);
            }
    
            if ($this->customer === null && post('nama') != null)
                $this->customer = new Customer();
    
            if (post('nama') != null)
                $this->customer->nama = post('nama');
            if (post('alamat') != null)
                $this->customer->alamat = post('alamat');
            if (post('telepon') != null)
                $this->customer->telepon = post('telepon');
            if (post('email') != null)
                $this->customer->email = post('email');
            if (post('keterangan') != null)
                $this->customer->keterangan = post('keterangan');
    
            if ($this->customer !== null){
                $this->customer->save();
                $log = new LogAktifitas();
                $log->kategori = "Customer";
                $log->user_id = $this->user->id;
                $log->data = json_encode($this->customer);
                $log->save();
            }

            $this->customers = Customer::get();
        }else{
            $this->customers = Customer::get();
        }
            
    }
}
