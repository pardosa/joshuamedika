<?php namespace Medika\Joshua\Components;

use Cms\Classes\ComponentBase;
use Medika\Joshua\Models\Penjualan;
use Auth;

class InvoiceFinal extends ComponentBase
{
    public $invoice;
    public $user;

    public function componentDetails()
    {
        return [
            'name'        => 'InvoiceFinal Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->user = Auth::getUser();

        $jualId = $this->param('jual_id');

        $this->invoice = Penjualan::find($jualId);
    }
}
