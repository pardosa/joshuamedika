<?php namespace Medika\Joshua\Components;

use Cms\Classes\ComponentBase;
use Medika\Joshua\Models\Cart as CartModel;
use Medika\Joshua\Models\Catalog;
use Auth;
use Redirect;

class Cart extends ComponentBase
{
    private $user;
    public $carts;

    public function componentDetails()
    {
        return [
            'name'        => 'Cart Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->user = Auth::getUser();

        $catId = $this->param('catalog_id');
        $opsi = $this->param('opsi');
        if ($catId != null && $opsi != null){    
            $this->updateProductToCart($catId, $opsi);
            return Redirect::to('/penjualan#myCart');
        }
        
        $this->carts = CartModel::where('status', '=',0)
            ->where('user_id','=',$this->user->id)
            ->orderBy('id','asc')->with('catalog','user')
            ->get();
            // echo "<pre>";
            // var_dump($this->carts[0]->catalog[0]['kode']);
            // echo "</pre>";
    }

    public function updateProductToCart($id, $opsi){
        $cart = CartModel::where('user_id','=',$this->user->id)
            ->where('catalog_id','=',$id)
            ->where('status','=',0)
            ->first();
        $produk = Catalog::find($id);
        //var_dump($produk->inventory->aktual);die;
        
        if ($opsi == "add"){
        
            if ($cart === null){
                $cart = new CartModel();
                $cart->catalog_id = $id;
                $cart->user_id = $this->user->id;
                $cart->status = 0;
                $cart->jumlah = 0;
            }
                //echo $produk->inventory->aktual;
            if($produk->inventory->aktual >= $cart->jumlah+1)
                $cart->jumlah = $cart->jumlah + 1;

            $cart->save();
        }else{
            if ($cart !== null){
                if ($produk->inventory->aktual < $cart->jumlah)
                    $cart->jumlah = $cart->catalog->inventory->aktual;

                if ($cart->jumlah > 1){
                    $cart->jumlah = $cart->jumlah - 1;
                    $cart->save();
                }else{
                    $cart->delete();
                }
            }
        }
        
    }
}
