<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaTrPembelianProduk extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_tr_pembelian_produk', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->integer('pembelian_id');
            $table->integer('catalog_id');
            $table->integer('jumlah');
            $table->integer('harga_beli');
            $table->integer('total');
            $table->string('keterangan');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_tr_pembelian_produk');
    }
}
