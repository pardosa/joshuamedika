<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaBrand extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_brand', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nama', 50);
            $table->string('kode', 5);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_brand');
    }
}
