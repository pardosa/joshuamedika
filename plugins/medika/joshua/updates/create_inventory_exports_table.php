<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateInventoryExportsTable extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_inventory_exports', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('medika_joshua_inventory_exports');
    }
}
