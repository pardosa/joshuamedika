<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaCustomer extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_customer', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nama', 50);
            $table->string('email', 20)->nullable();
            $table->string('alamat', 100);
            $table->string('telepon', 15);
            $table->string('keterangan', 50)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_customer');
    }
}
