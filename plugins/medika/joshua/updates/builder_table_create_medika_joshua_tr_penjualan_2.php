<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaTrPenjualan2 extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_tr_penjualan', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('no_faktur', 15);
            $table->integer('customer_id');
            $table->integer('user_id');
            $table->integer('total');
            $table->string('keterangan', 100);
            $table->integer('status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_tr_penjualan');
    }
}
