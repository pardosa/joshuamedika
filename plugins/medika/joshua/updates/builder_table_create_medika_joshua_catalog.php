<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaCatalog extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_catalog', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('kode', 15);
            $table->string('nama', 50);
            $table->string('ukuran', 30)->nullable();
            $table->dateTime('tgl_produksi')->nullable();
            $table->dateTime('tgl_expire')->nullable();
            $table->string('harga');
            $table->string('modal');
            $table->string('keterangan', 150);
            $table->integer('inventory_id')->nullable();
            $table->integer('brand_id')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_catalog');
    }
}
