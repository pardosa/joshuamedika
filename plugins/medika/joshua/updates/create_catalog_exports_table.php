<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCatalogExportsTable extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_catalog_exports', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('medika_joshua_catalog_exports');
    }
}
