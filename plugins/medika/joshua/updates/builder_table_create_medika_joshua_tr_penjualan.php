<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaTrPenjualan extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_tr_penjualan', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('no_faktur', 15);
            $table->string('catalog_id');
            $table->integer('jumlah');
            $table->integer('harga_beli');
            $table->integer('harga_jual');
            $table->string('keterangan', 50);
            $table->integer('customer_id');
            $table->integer('user');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_tr_penjualan');
    }
}
