<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaTrPembelian extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_tr_pembelian', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('no_faktur', 15);
            $table->integer('supplier_id');
            $table->integer('user_id');
            $table->integer('total');
            $table->string('keterangan');
            $table->integer('status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_tr_pembelian');
    }
}
