<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaInventory extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_inventory', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('aktual')->default(0);
            $table->integer('maksimum')->default(0);
            $table->integer('minimum')->default(0);
            $table->string('lokasi');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_inventory');
    }
}
