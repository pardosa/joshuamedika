<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaTrInventory extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_tr_inventory', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('no_faktur', 15);
            $table->integer('supplier');
            $table->integer('user');
            $table->integer('jumlah');
            $table->string('keterangan', 100);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_tr_inventory');
    }
}
