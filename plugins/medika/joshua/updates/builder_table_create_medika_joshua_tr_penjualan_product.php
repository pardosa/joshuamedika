<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaTrPenjualanProduct extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_tr_penjualan_product', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('penjualan_id')->nullable();
            $table->integer('catalog_id')->nullable();
            $table->integer('jumlah');
            $table->integer('harga_beli');
            $table->integer('harga_jual');
            $table->integer('total');
            $table->string('keterangan', 100)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->primary(['user_id', 'role_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_tr_penjualan_product');
    }
}
