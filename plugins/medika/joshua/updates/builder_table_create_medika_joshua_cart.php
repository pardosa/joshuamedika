<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaCart extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_cart', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('catalog_id');
            $table->integer('user_id');
            $table->integer('jumlah');
            $table->integer('status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_cart');
    }
}
