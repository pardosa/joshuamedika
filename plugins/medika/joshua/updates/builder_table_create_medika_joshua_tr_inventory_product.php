<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaTrInventoryProduct extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_tr_inventory_product', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('trans_id');
            $table->integer('catalog_id');
            $table->integer('jumlah');
            $table->integer('harga');
            $table->string('keterangan', 100)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_tr_inventory_product');
    }
}
