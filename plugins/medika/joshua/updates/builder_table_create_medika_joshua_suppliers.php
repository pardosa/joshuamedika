<?php namespace Medika\Joshua\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMedikaJoshuaSuppliers extends Migration
{
    public function up()
    {
        Schema::create('medika_joshua_suppliers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('brand', 50);
            $table->string('nama', 50);
            $table->string('email', 30)->nullable();
            $table->string('telepon1', 15);
            $table->string('telepon2', 15)->nullable();
            $table->string('alamat', 100)->nullable();
            $table->string('kota', 20);
            $table->string('keterangan', 255);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('medika_joshua_suppliers');
    }
}
