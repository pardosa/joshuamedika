<?php namespace Medika\Joshua\Models;

use Model;

/**
 * Model
 */
class Pembelian extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medika_joshua_tr_pembelian';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'pembelianproduk' => ['Medika\Joshua\Models\PembelianProduk'],
    ];

    public $belongsTo = [
        'supplier' => ['Medika\Joshua\Models\Supplier'],
        'user' => ['RainLab\User\Models\User'],
    ];

    public function scopeisMine($query, $user){

        return $query->where('user_id', $user);
        
    }

    public function scopeByFaktur($query,$val){
        return $query->where('no_faktur', $val);
    }
}
