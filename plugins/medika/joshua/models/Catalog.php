<?php namespace Medika\Joshua\Models;

use Model;

/**
 * Model
 */
class Catalog extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medika_joshua_catalog';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'supplier' => ['Medika\Joshua\Models\Supplier'],
        'inventory' => ['Medika\Joshua\Models\Inventory'],
        'brand' => ['Medika\Joshua\Models\Brand']
    ];

    // public $hasOne = [
    //     'brand' => ['Medika\Joshua\Models\Brand','key' => 'id', 'otherKey' => 'brand_id'],
    // ];

}
