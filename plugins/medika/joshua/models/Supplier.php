<?php namespace Medika\Joshua\Models;

use Model;

/**
 * Model
 */
class Supplier extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medika_joshua_suppliers';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'penjualan' => 'Medika\Joshua\Models\Inventory',
        'catalog' => 'Medika\Joshua\Models\Catalog'
    ];
}
