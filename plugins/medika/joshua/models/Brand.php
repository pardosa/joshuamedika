<?php namespace Medika\Joshua\Models;

use Model;

/**
 * Model
 */
class Brand extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medika_joshua_brand';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
