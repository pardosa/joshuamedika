<?php namespace Medika\Joshua\Models;

use Model;

/**
 * Model
 */
class PenjualanProduk extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medika_joshua_tr_penjualan_product';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'penjualan' => ['Medika\Joshua\Models\Penjualan'],
        'catalog' => ['Medika\Joshua\Models\Catalog']
    ];

    // public $hasOne = [
    //     'catalog' => 'Medika\Joshua\Models\Catalog'
    // ];
}
