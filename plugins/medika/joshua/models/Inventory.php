<?php namespace Medika\Joshua\Models;

use Model;

/**
 * Model
 */
class Inventory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medika_joshua_inventory';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasOne = [
        'catalog' => 'Medika\Joshua\Models\Catalog'
    ];

    
}
