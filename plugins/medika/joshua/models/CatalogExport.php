<?php namespace Medika\Joshua\Models;

use Model;

/**
 * CatalogExport Model
 */
class CatalogExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        $catalogs = Catalog::with([
            'brand' => function($query){ $query->addSelect(['nama']); },
            'supplier' => function($query){ $query->addSelect(['nama']); },
            'inventory' => function($query){ $query->addSelect(['aktual', 'lokasi']); },
        ])->get();
        
        $catalogs->each(function($catalog) use ($columns) {
            $catalog->addVisible($columns);
        });
        
        $collection = collect($catalogs->toArray());
        $data = $collection->map(function ($item) {
            if(is_array($item)){
                foreach($item as $key => $value) {
                    if(is_array($value)) {
                        $item[$key] = json_encode($value);
                    }
                }
            }
            return $item;
        });
        var_dump($data);die;
        return $data->toArray();
    }
}
