<?php namespace Medika\Joshua\Models;

use Model;

/**
 * Model
 */
class PembelianProduk extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medika_joshua_tr_pembelian_produk';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'pembelian' => ['Medika\Joshua\Models\Pembelian'],
        'catalog' => ['Medika\Joshua\Models\Catalog']
    ];
}
