<?php namespace Medika\Joshua\Models;

use Model;

/**
 * Model
 */
class Customer extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medika_joshua_customer';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'pembelians' => 'Medika\Joshua\Models\Penjualan'
    ];
}
