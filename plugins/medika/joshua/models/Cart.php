<?php namespace Medika\Joshua\Models;

use Model;

/**
 * Model
 */
class Cart extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medika_joshua_cart';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasOne = [
        'user' => ['RainLab\User\Models\User','key' => 'id', 'otherKey' => 'user_id'],
        'catalog' => ['Medika\Joshua\Models\Catalog','key' => 'id', 'otherKey' => 'catalog_id']
    ];
}
