<?php namespace Medika\Joshua\Models;

use Model;

/**
 * Model
 */
class Penjualan extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'medika_joshua_tr_penjualan';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'penjualanproduk' => ['Medika\Joshua\Models\PenjualanProduk'],
    ];

    public $belongsTo = [
        'customer' => ['Medika\Joshua\Models\Customer'],
        'user' => ['RainLab\User\Models\User'],
    ];

    public function scopeisMine($query, $user){

        return $query->where('user_id', $user);
        
    }

    public function scopeByFaktur($query,$val){
        return $query->where('no_faktur', $val);
    }

    public function scopeFilterMonth($query, $filter)
    {
        $query->whereMonth('created_at', '=', date('m'));
    }

   
}
