<?php namespace Medika\Joshua\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Vdomah\Excel\Classes\Excel;
use Medika\Joshua\Models\InventoryExport;

class Inventory extends Controller
{
    public $implement = [        
        'Backend\Behaviors\ListController',        
        'Backend\Behaviors\FormController',        
        'Backend\Behaviors\ReorderController',
        'Backend\Behaviors\RelationController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Medika.Joshua', 'main-data', 'side-menu-inventory');
    }

    // public function export() 
    // {
    //     return Excel::export(new InventoryExport, 'inventories.xlsx');
    // }

}
