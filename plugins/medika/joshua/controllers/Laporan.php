<?php namespace Medika\Joshua\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Request;
use ApplicationException;
use Log;
use Medika\Joshua\Models\Penjualan;
use RainLab\User\Models\User;

class Laporan extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController', 'Backend\Behaviors\RelationController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    private $months = ['Semua','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Medika.Joshua', 'main-laporan');

        $this->vars['kasirs'] = User::all();
    }

    public function onCariTransaksi()
    {
        
        if (Request::input('bulan') == '') {
            throw new ApplicationException("Bulan harus dipilih");
        }
        if (Request::input('tahun') == '') {
            throw new ApplicationException("Tahun harus dipilih");
        }

        if (Request::input('bulan')!='0' && Request::input('tahun') != '0'){
            $penjualans = Penjualan::where('status',0)
                        ->whereMonth('created_at', '=', Request::input('bulan'))
                        ->whereYear('created_at', '=', Request::input('tahun'));
        }elseif(Request::input('bulan') =='0' && Request::input('tahun') != '0'){
            $penjualans = Penjualan::where('status',0)
                        ->whereYear('created_at', '=', Request::input('tahun'));
        }else{
            $penjualans = Penjualan::where('status',0);
        }

        if (Request::input('kasir') != '0'){
            $penjualans = $penjualans->where('user_id',Request::input('kasir'));
        }
        $penjualans = $penjualans->get();

        Log::info('onCariTransaksi: '. json_encode($penjualans));
        $this->vars['bulan'] = $this->months[Request::input('bulan')];
        $this->vars['tahun'] = Request::input('tahun');
        $this->vars['penjualans'] = $penjualans;
    
        return [
            'laporan' => $this->makePartial('laporan')
        ];
        
    }
}
