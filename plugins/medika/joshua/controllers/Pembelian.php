<?php namespace Medika\Joshua\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Db;
use Redirect;

class Pembelian extends Controller
{
    public $implement = [        
        'Backend\Behaviors\ListController',        
        'Backend\Behaviors\FormController',        
        'Backend\Behaviors\ReorderController',
        'Backend\Behaviors\RelationController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Medika.Joshua', 'main-transaksi', 'side-menu-Pembelian');
    }

    public function listExtendQuery($query)
    {
        if (!$this->user->hasAccess('medika.joshua.admin')){
            $query->isMine($this->user->id);
        }
    }

    public function onPrint()
    {
        $no_faktur = post("Pembelian")["no_faktur"];
        $penjualan = Db::table('medika_joshua_tr_pembelian')->where('no_faktur', $no_faktur)->update(['status' => 1]);
        //$penjualan = PenjualanModel::byFaktur($no_faktur);
        //Log::info('PenjualanModel::findByFaktur => '.json_encode($penjualan));
        
        //throw new ApplicationException(json_encode($penjualan));
        return Redirect::refresh();
        
    }
}
