<?php namespace Medika\Joshua\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class LogActivities extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Medika.Joshua', 'main-laporan', 'side-menu-aktifitas');
    }
}
